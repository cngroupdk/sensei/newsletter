# Sensei Newsletter no 2.

## Topics for this newsletter:

1. Newsletter improvements
2. New and worthy to try/follow in 2022
3. Articles & links

## 1. Newsletter improvements

You have probably noticed this NL is different from the previous one. Based on feedback from you, we are now sending (hopefully nice) HTML email instead of poorly formatted text via Outlook. 
We have developed simple markdown to sensei email-HTML tool. If interested you can check it on [sensei gitlab](https://gitlab.com/cngroupdk/sensei/markdown-to-nl/-/blob/main/src/routes/index.svelte). It is very simple or even naive, but considered it was done in like 4 hours, it is just enough for our needs.

## 2. New and worthy to try/follow in 2022

Based on [State of JS survey](https://2021.stateofjs.com/en-US/), [State of CSS survey](https://2021.stateofcss.com/en-US/), [Rising stars stats](https://risingstars.js.org/2021/en), our experience and knowledge, we have picked some technologies and knowledge sources, which are worthy to follow in 2022. If you start new your project in CN you can consider following technologies as possible candidates beside those we already use in CN. But please, if you choose one of those new technologies, which haven't been used in CN so far, try to consult your choices with Sensei group!


**FE Frameworks with potential**
- [Solid](https://www.solidjs.com/) - More less react with fundamental difference. It compiles to real DOM and does not use virtual DOM, so the performance could be way higher for some apps
- [Svelte](https://svelte.dev/) - Compiled framework with minimal bundle size. Out of box transitions, animations, first class reactivity variables, state machine.

**BE Frameworks with potential**
- [SvelteKit](https://kit.svelte.dev/) - Application framework for Svelte
- [Fastify](https://www.fastify.io/) - Way faster alternative to express, unfortunately the ecosystem is still not the huge.
- [nest.js](https://nestjs.com/) - Object oriented. Heavily uses decorators. Can use fastify as connector out of the box.

**Testing**
- [Testing Library](https://testing-library.com/)
- [Cypress](https://www.cypress.io/)
- [Playwright](https://playwright.dev/)

**Build tools**
- [Vite](https://vitejs.dev/) - Super fast "bundler" with hot module replacement. Built on top of Esbuild. If you would like to hear more there is ["A deep-dive on Vite"](https://changelog.com/jsparty/212) podcast with author of Vite.
- [Esbuild](https://esbuild.github.io/) - An extremely fast JavaScript bundler :)
- [SWC](https://swc.rs/) - Rust based bundler. Babel alternative, [used in Next.js](https://nextjs.org/docs/advanced-features/compiler), deno, ...

**Monorepo**
- [Nx](https://nx.dev/) - Next generation build system with first class monorepo support and powerful integrations.


**Libraries**
- [Date-fns](https://date-fns.org/) - Simple date handling library
- [day-js](https://day.js.org/) - Similar API like moment.js but way smaller bundle
- [i18nJS](https://github.com/fnando/i18n-js) - Export i18n translations to JSON.


**Other**
- [Supabase](https://supabase.com/) - Open source Firebase alternative. If you would like to know more about it there is ["Supabase is all in on Postgres"](https://changelog.com/podcast/476) podcast with Supabase's creator.
- [zx](https://github.com/google/zx) - Shell scripting with Node

**UI frameworks**
- [Chakra UI](https://chakra-ui.com/) - React UI components
- [Headless UI](https://headlessui.dev/) - UI components for React and Vue

**CSS**
- [Tailwind v3](https://tailwindcss.com/blog/tailwindcss-v3) - Probably the biggest change and step forward from the initial Tailwind release.
- [CSSnano](https://cssnano.co/) - This one is becoming an industry standard for CSS minification (PostCSS plugin)
- [Vanilla extract](https://vanilla-extract.style/) - Not used much so far, but widely anticipated CSS-in-JS tool. It builds your css in build time and save your app from style related runtime computation.

Further we recommend some magazines / podcasts and influential people to follow. These sources should be just fine for your self education and for staying informed about latest trends.

**Blogs and Magazines**
- [CSS-Tricks](https://css-tricks.com/)
- [Smashing magazine](https://www.smashingmagazine.com/)
- [Javascript weekly](https://javascriptweekly.com/) - Newsletter with news from world of javascript

**Podcasts**
- [Syntax](https://syntax.fm/)
- [JS Party](https://open.spotify.com/show/2ySVrxGkN6n6frMTo9Nsrt)
- [The Changelog](https://changelog.com/podcast)
- [HTTP 203](https://www.youtube.com/playlist?list=PLNYkxOF6rcIAKIQFsNbV0JDws_G_bnNo9)

**Most influential people to follow**
- Dan Abramov
- Kent C. Dodds
- Evan You
- Rich Harris


## 3. Articles & links

- [Google ZX Shell scripts in node](https://www.sitepoint.com/google-zx-write-node-shell-scripts/)
- [Remix vs Next.js by Ryan Florence](https://remix.run/blog/remix-vs-next)
- [JavaScript debugging guide by Flavio Copes](https://flaviocopes.com/debugging/)
- [Vue 3 as the New Default](https://blog.vuejs.org/posts/vue-3-as-the-new-default.html)
- [Memory leaks: the forgotten side of web performance](https://nolanlawson.com/2022/01/05/memory-leaks-the-forgotten-side-of-web-performance/)
- [/Possible/ Future Javascript: Records and Tuples](https://dev.to/smpnjn/future-javascript-records-and-tuples-14fk)
- [New possibilities with Next.js 12.1](https://nextjs.org/blog/next-12-1)

If you need any help with app architecture, picking right solution/library for you project or just getting feedback, don't hesitate and contact us on Teams **Sensei - public** channel.
