# Sensei Newsletter no 4.

## Topics for this newsletter:

1. DevOps are joining Sensei NL
2. Web & Mobile News
3. Articles & links
4. Upcoming Events

## 1. DevOps are joining Sensei NL

Maybe you have noticed, that DevOps group is getting more active within CN. Because they have a lot to share, we decided to join our forces for this and upcoming newsletters. You can look forward to news from DevOps community.

Say hello to DevOps team and welcome: Jiří Rejman, Jiří Leinveber, Jakub Buriánek and Petter Rossa.

Together we are introducing a new newsletter section **Upcoming Events**. It will list upcoming education events in CN (hopefully including Ciklum soon) related to Web development and DevOps.

## 2. Web & Mobile News

Quite a lot has happened in the past few months. Introduction of JS runtime [Bun](https://bun.sh/) triggered a race between JS runtimes. Node js started to completely rebuild its simple server implementation to catch up with a performance boost of Bun. And node also finally implemented a watch mode. No more need for the nodemon package! Deno was encouraged to introduce compatibility with node.js to provide a broader package environment. Interestingly Bun is based on JavaScripCore (webKit) compared to the competitors based on V8 engine. More competitors will certainly bring faster iterations and performance.

Decorators are coming to JS. You may know them from other languages, even Typescript has had them in experimental mode for quite some time. If you don't know decorators/annotations you should [check them here](https://2ality.com/2022/10/javascript-decorators.html). They certainly will bring a revolution to JavaScript world.

### React

#### Next 13

After a long time waited, the Vercel team have finally introduced a new future way how to work with Next.js.
New version of Next opens the door for big changes across the whole ecosystem which solves long-standing issues.
In cooperation with React team, they have decided to use certain features that React introduced in version 18 such as "Server components", first-class support for Promises and more!

If you have ever wished to use nested layouts, streaming or wanted to get rid of the tough Image component styling, now is the time.

Even though this change is backward compatible and there might be small changes, we recommend reading the article and if you have more time to spare, watch the Next.js conference record.

- [Article](https://nextjs.org/blog/next-13)
- [Conference](https://nextjs.org/conf)

### Google and Android 2022

What a journey! So many things happened this year. The android team officially released Jetpack Compose stable version! It is a modern android UI toolkit allowing a declarative approach. Also, Jetpack Compose for WearOS has been released recently. Multi-module application is not something new to android apps. However, there wasn’t any official guidance on how to do it. Google realized it and came up with a set of [recommendations](https://android-developers.googleblog.com/2022/09/announcing-new-guide-to-android-app-modularization.html). During the last summer and autumn, the DroidCon, the biggest android conference was organized again this year. There are many great [topics](https://www.droidcon.com/content/) to watch during tough winter days. We recommend [Dynamic Code with Zipline](https://www.droidcon.com/2022/09/29/dynamic-code-with-zipline/) and/or [Dynamic Navigation in Modularised Apps](https://www.droidcon.com/2022/11/15/breaking-the-rules-dynamic-navigation-in-modularized-apps-2/)

- [DroidCon - public content](https://www.droidcon.com/content/)

### Kotlin Multiplatform Mobile

We should also not forget this topic. As you probably know, KMM is a new cross-platform framework allowing to share of the logic between platforms (e.g. Android and iOS) while keeping the UX native. Many practical topics were discussed also on mentioned DroidCon conferences (pros/cons, traps) and the interest in this technology was extraordinary. We can say that KMM is on a rise and it’s stronger and stronger by the day within the community. KMM is already in Beta by the way.

## 3. Articles & links

### Web & Mobile

- [Qwik](https://www.youtube.com/watch?v=0dC11DMR3fU&t=279s) - Introduction of a new FE framework. An interesting new take on how future FE frameworks could work.
- [Turbopack](https://vercel.com/blog/turbopack) - New Rust-based bundling tool that even beats Vite - currently in Alpha
- [The new wave of Javascript web frameworks](https://frontendmastery.com/posts/the-new-wave-of-javascript-web-frameworks/) - what are the new frameworks? Where the FE development is heading? You will get the answers.
- [State of CSS](https://stateofcss.com/en-us/) - Take the survey and learn what is new in CSS in 2022
- [Container Queries](https://css-tricks.com/early-days-of-container-style-queries/) - just landed in your browser
- [Jetpack Compose](https://developer.android.com/jetpack/compose)
- [Android 13 Deep Dive](https://blog.esper.io/android-13-deep-dive/)
- [What’s new-Material Design 1.7.0](https://navczydev.medium.com/whats-new-material-design-1-7-0-18c3eabb618b)
- [Jetpack Compose Tooling](https://medium.com/androiddevelopers/compose-tooling-42621bd8719b)
- [Official KMM documentation](https://kotlinlang.org/lp/mobile/)
- [KMM intro](https://medium.com/@fraagurod/our-journey-to-kotlin-multiplatform-101d36b4cf43)

### DevOps

- [DevOps is a failure](https://leebriggs.co.uk/blog/2022/06/21/devops-is-a-failure) In the first edition of the Article club (it will return just after Christmas) we went over this article. If you missed it check it out, it has some interesting ideas inside and maybe check the discussion [here](https://ciklum.workplace.com/groups/cloudarchitectureguild/permalink/2117614135115218/).
- [DevOps War Story](https://theagileadmin.com/2022/06/29/devops-war-story-the-vmware-realization/) A story from the days that DevOps was not called DevOps. How one team discovered the benefits of having them responsible for product end-to-end.
- [AWS CDK](https://aws.amazon.com/cdk/) I had the pleasure of working with AWS CDK for the last few months and I would never look back. If our languages are good enough for the application, then why not for infrastructure?
- [dagger.io](https://dagger.io/) An interesting new way to write CI/CD pipelines. You write the pipeline in the language of your choice (if your choice is CUE, GO, or Python), and using their CDK you build your pipeline. One of the biggest advantages is that you can run your pipelines locally.

## 4. Upcoming Events

- January 2022 - RxJs Sensei meetup. Tomáš Řezáč will introduce rxjs (reactiveX streams API) and show some interesting use cases or even architectural solutions.

If you need any help with app architecture, picking the right solution/library for your project or just getting feedback, don't hesitate and contact us on Teams **Sensei - public** or **DevOps** channels.
