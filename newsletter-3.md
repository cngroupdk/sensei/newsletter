# Sensei Newsletter no 3.

## Topics for this newsletter:

1. Dev talks
2. Pet Projects - tips and hints
3. Dev news
4. Articles & links

## 1. Dev talks

We hope you all enjoyed Zlin Dev talks 2022. We are back with your feedback evaluation. Thanks to all who participated. All talks were rated by at least 2 out of 3 stars on average - good you liked it. The top 3 were 'UX Accessibility', 'Design Systems' and 'Pet projects'. Looks like lighter topics (more visual ones) were preferred over hardcore coding. In the suggestion part of the feedback form, you most often mentioned, that the titles of presentations were not always well describing their content and you would appreciate a brief description of talks. We will try to provide it next time.
As a talk with the best feedback was 'Pet projects' I'll try to push it again next year. I'll also recap here, why it is a great idea to have your pet project.

## 2. Pet Projects - tips and hints

Based on the success of 'Pet projects' at Dev Talks, We are back with this topic.

**Why should you have a pet project?**

1. It is fun.
2. It is the best way how to learn new technologies.
3. It will increase your programming self-confidence.
4. You can do what you like. None force you to code something you don't agree with.

**Some hints for starting a new pet project**

1. Choose a topic you enjoy (a game or tool can be a good choice). If you choose something others will enjoy as well, there is a higher chance of positive feedback and your satisfaction.
2. Choose new technology, so you learn something new
3. Try starting with a small and simple project. You should be done with [MVP](https://en.wikipedia.org/wiki/Minimum_viable_product) in tens of hours. Fine-tuning will take much more time if you decide to follow up.
4. Don't try to start a project, which is way beyond your skills, it would just cause frustration.
5. Combine reading of articles on chosen technology with coding.
6. If you are not happy with your project at any point don't be afraid to start over or throw it away and start with something different.
7. Be prepared for failure and or success.

**Ideas for pet projects**

- Remember some of those old-school games, like 2d dog fights, Sokoban or Tyrian. With such games, you can learn requestAnimationFrame API, Vectors and Velocity concepts, Animations and more.
- Do you know, JS can create sounds? You can write a music composer or piano quite easily. Or maybe a music player.
- Code a Photo gallery to show your photos online in style. Learn about Avif, WebP, Jpeg XL formats and responsive images.
- What about a meme generator using canvas API?

You can probably come up with more interesting ideas...

## 3. Dev News

### React

- React 18 is here for a few months yet. Read more about new features and improvements in [React 18 blog post](https://reactjs.org/blog/2022/03/29/react-v18.html)

### iOS

- You can now program in SwiftUI on your iPad - Swift Playgrounds 4 (distribution on AppStore Connect / TestFlight!)
- async/await just landed in Swift
- CI/CD is coming in [Xcode](https://developer.apple.com/documentation/xcode/xcode-cloud) Cloud](https://developer.apple.com/documentation/xcode/xcode-cloud)

### JS

- Best in class FE tooling/bundler Vite is here with [Version 3](https://vitejs.dev/blog/announcing-vite3.html)
- Bun, a new JS runtime is grabbing attention. First benchmarks show it is way faster than Node or Deno. Let's see if it does it to enterprise solutions. Check [bun.sh](https://bun.sh/)

## 4. Articles & links

- [The Complete Guide to Regular Expressions (Regex)](https://dev.to/coderpad/the-complete-guide-to-regular-expressions-regex-1m6)
- [Javascript function composition: what’s the big deal?](https://jrsinclair.com/articles/2022/javascript-function-composition-whats-the-big-deal/)
- [Tao of Node](https://alexkondov.com/tao-of-node/). Lengthy but highly recommended article for node.js developers
- [Typescript 4.7](https://devblogs.microsoft.com/typescript/announcing-typescript-4-7/?q=1) is out.
- [A Proposal For Type Syntax in JavaScript](https://devblogs.microsoft.com/typescript/a-proposal-for-type-syntax-in-javascript/)
- [Next.js Layouts RFC](https://nextjs.org/blog/layouts-rfc)

If you need any help with app architecture, picking the right solution/library for your project or just getting feedback, don't hesitate and contact us on Teams **Sensei - public** channel.
